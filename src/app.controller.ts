import { Controller, Get, Post, Body, Patch, Param, Delete,UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { get } from 'http';
import { RegisterAuthDto } from './auth/dto/register.dto';
import { JwtGuard } from './utility/jwt.guard';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get("/greeting")
  async getHello() {
    return this.appService.getHello();
  }

  @Get("/greeting-async")
  async getHelloAsync() {
    return this.appService.getHelloAsync();
  }

  @Get("/publish-event")
  async publishEvent() {
    this.appService.publishEvent();
  }
  @Get('/prueba')
  async getmesage(){
    this.appService.getmensaje();
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  //esto sera para los users


  //prueba post
  @Post('/cabra')
  async getCabra(){
    this.appService.getCabra();
  }


  //mover al controller de user luego
  //retorna todos los usuarios
  @UseGuards(JwtGuard)
  @Get('/users')
  async findAll() {
    return this.appService.findAll();
  }

  //retorna un usuario
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.appService.findOne(+id);
  }

  //la prueba si funciona,se guarda un usuario
  @Post("/singIn")
  async singIn(@Body() body:any) {
    return this.appService.singIn(body);
  }

   //pruebas para conectar user
   @Post("/singup")
   async singup(@Body() body:RegisterAuthDto) {
     return  this.appService.singup(body);
   }

}