import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { JwtStrategy } from './utility/jwt.strategy';

@Module({
  imports: [
    ClientsModule.register([{
      name: 'GREETING_SERVICE',
      transport: Transport.RMQ,
      options: {
        urls: ['amqp://localhost:5672'],
        queue: 'books_queue',
        queueOptions: {
          durable: false
        }
      }
    }]),
    AuthModule
  ],
  controllers: [AppController],
  providers: [AppService,JwtStrategy],
})
export class AppModule {}