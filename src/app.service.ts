import { Body, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { RegisterAuthDto } from './auth/dto/register.dto';

@Injectable()
export class AppService {

  constructor(@Inject('GREETING_SERVICE') private client: ClientProxy){}

  async getHello(){
    return this.client.send({cmd: 'greeting'}, 'Progressive Coder');
    //ejecutara el return del microservicio
  }

  async getHelloAsync() {
    const message = await this.client.send({cmd: 'greeting-async'}, 'Progressive Coder enviado desde cliente');
    //el send es una funcion de envio
    return message;//cuando ejecuto gethelloasync
    //send.cmd busca en la cola la funcion con el comando greeting

  }

  async publishEvent() {
    this.client.emit('book-created', {'bookName': 'The Way Of Kings', 'author': 'Brandon Sanderson'});
    //envia info al microservicio pero no ejecuta el return
  }
 
  async getmensaje(){
    const message =await this.client.emit('proyectox', 'desinfectante');
    return message;
  }

  

  async getCabra(){
    return this.client.send('cabra','prueba de Post Cabra');
    //el microservicio espera la palabra clave cabra
  }


  //mover al service de user luego
  //retorna todos los usuarios
  async findAll(){
    return this.client.send('findAllUsers','');
  }

  async findOne(id: number) {
    return this.client.send('findOneUser',id);
  }

  //login
  async singIn(body:any) {
    return this.client.send('singIn', body);
  }

  //register
  async singup(body:RegisterAuthDto) {
    return this.client.send('singUp',body);
  }
  
}